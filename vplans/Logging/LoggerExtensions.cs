﻿using Serilog;
using System;

namespace vplans.Logging
{
    public static class LoggerExtensions
    {
        public static void Info(this ILogger log, string messageTemplate) => log.Information(messageTemplate);
        public static void Info(this ILogger log, Exception loggable) => log.Information($"{loggable}");
        public static void Error(this ILogger log, object loggable) => log.Error($"{loggable}");
    }
}
