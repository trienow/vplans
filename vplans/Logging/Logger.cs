﻿using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;

namespace vplans.Logging
{
    internal static class Logger
    {
        public static ILogger Instance { get; private set; }

        /// <summary>
        /// Sets up a logger with default values, or values loaded from the config file.
        /// </summary>
        /// <param name="cfg">An instance of the config class. May be <see langword="null"/>.</param>
        public static void SetupLogger(Configuration.Config cfg)
        {
            const string outputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}"; //Message:lj is default

            int retainedFileCountLimit = 12;
            RollingInterval rollingInterval = RollingInterval.Month;
            LogEventLevel systemLevel = LogEventLevel.Warning;

            //Only use the configuration, if it surely is not null!
            if (cfg is Configuration.Config)
            {
                if (cfg.OperatorIdIsSet)
                {
                    retainedFileCountLimit = 2;
                    rollingInterval = RollingInterval.Day;
                }

                if (cfg.LogDebugEvents)
                {
                    systemLevel = LogEventLevel.Debug;
                }
            }

            Instance = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(@"logs\error.log", restrictedToMinimumLevel: LogEventLevel.Error, outputTemplate: outputTemplate, fileSizeLimitBytes: 20_000_000, rollOnFileSizeLimit: true, rollingInterval: rollingInterval, retainedFileCountLimit: retainedFileCountLimit, buffered: false, shared: true)
                .WriteTo.File(@"logs\system.log", restrictedToMinimumLevel: systemLevel, outputTemplate: outputTemplate, fileSizeLimitBytes: 20_000_000, rollOnFileSizeLimit: true, rollingInterval: RollingInterval.Day, retainedFileCountLimit: 10, buffered: true, flushToDiskInterval: new TimeSpan(0, 1, 0))
                .CreateLogger();
            Instance.Information("Logger Online");
        }

        public static IEnumerable<string> GetErrorFiles()
        {
            List<string> files = new List<string>(2);

            if (Directory.Exists(@"logs\"))
            {
                DateTime today = DateTime.Now.Date;
                foreach (string file in Directory.EnumerateFiles(@"logs\", "err*.log", SearchOption.TopDirectoryOnly))
                {
                    if (new FileInfo(file).LastWriteTime < today)
                    {
                        files.Add(file);
                    }
                }
            }

            return files;
        }
    }
}
