﻿using Serilog;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using vplans.Configuration;
using vplans.DB;
using vplans.Logging;
using vplans.Telegrammer;
using vplans.Vertretungsplan;

namespace vplans.Logic
{
    public class Communicator
    {
        private readonly ILogger logger = Logger.Instance;
        private readonly Users users = Users.Instance;
        private readonly Config cfg = Config.GetInstance();
        private readonly VPlaner vplaner;
        private readonly VBot bot;

        public bool ShutdownRequested { get; private set; } = false;

        public Communicator(VPlaner vplaner)
        {
            this.vplaner = vplaner;
            this.bot = new VBot();
            this.bot.InboundEvent += this.Bot_InboundEvent;
            this.bot.StartBot();
        }

        public void NotifyClasses(string[] classes)
        {
            if (classes.Length > 0)
            {
                UserTable[] affectedUsers = users.GetUsersToRemind(classes);
                foreach (UserTable user in affectedUsers)
                {
                    OutboundMessage msg = new OutboundMessage(user, vplaner.GetVPlan(user.Class));
                    msg.InlineKeyboard = Keyboards.CallbackKlasse(user.Class);
                    bot.SendMessage(msg);
                }
            }
        }
        public void PurgeObsoleteUsers()
        {
            IDbTransaction transaction = DBCore.Instance.StartTransaction();

            UserTable[] affectedUsers = users.GetObsoleteUsers();
            foreach (UserTable user in affectedUsers)
            {
                OutboundMessage msg = new OutboundMessage(user.ID, Texts.GOODBYE);
                msg.ReplyKeyboard = Keyboards.START;
                bot.SendMessage(msg);
                users.DeleteUser(user);
            }

            affectedUsers = users.GetInvalidUsers();
            foreach (UserTable user in affectedUsers)
            {
                OutboundMessage msg = new OutboundMessage(user.ID, Texts.UPGRADE_INVALID_CLASS);
                msg.ReplyKeyboard = Keyboards.START;
                bot.SendMessage(msg);
                users.DeleteUser(user);
            }

            transaction.Commit();
        }
        public void SendDebugFile(string file)
        {
            OutboundMessage msg = new OutboundMessage(cfg.OperatorId);
            FileInfo fi = new FileInfo(file);
            if (fi.Length > 10_000_000)
            {
                msg.AddText($"The log file \"{Path.GetFileName(file)}\" is larger than 10MB!", null);
                using (FileStream fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (StreamReader sr = new StreamReader(fs))
                {
                    char[] buffer = new char[1024];
                    int readChars = sr.ReadBlock(buffer, 0, 1024);

                    StringBuilder builder = new StringBuilder(1024).Append(buffer, 0, readChars);
                    msg.AddText(builder.ToString(), null);

                    if (readChars == 1024)
                    {
                        readChars = sr.ReadBlock(buffer, 0, 1024);

                        builder = new StringBuilder(1024).Append(buffer, 0, readChars);
                        msg.AddText(builder.ToString(), null);
                    }
                }
            }
            else
            {
                msg.AddDocument(file);
            }


            bot.SendMessage(msg);
        }
        public void SendDebugText(string content)
        {
            OutboundMessage msg = new OutboundMessage(cfg.OperatorId);
            const int MAX_CHARS = 1024;

            for (int i = 0; i * MAX_CHARS < content.Length; i++)
            {
                int start = i * MAX_CHARS;
                int maxLen = Math.Min(content.Length - start, MAX_CHARS);
                string subtext = content.Substring(start, maxLen);
                msg.AddText(subtext, null);
            }

            bot.SendMessage(msg);
        }

        private void Bot_InboundEvent(object sender, InboundEventArgs e)
        {
            OutboundMessage msg = Respond(e.ChatId, e.Message);
            if (msg != null)
            {
                if (e.SendingQuery == null)
                {
                    this.bot.SendMessage(msg);
                }
                else
                {
                    msg.RespondingToQuery = e.SendingQuery;
                    this.bot.AnswerCallback(msg);
                }
            }
        }

        private OutboundMessage Respond(long chatId, string command = null)
        {
            UserTable user = users.GetUser(chatId);

            // --- ANTI SPAM ---
            bool mayProcessMessage = true;
            bool shouldIssueWarning = false;
            DateTime now = DateTime.Now;
            if (user.AntiSpam < now)
            {
                user.AntiSpam = now.AddMinutes(1); //<- Init
            }
            else if (user.AntiSpam > now.AddMinutes(3))
            {
                mayProcessMessage = false; //<- Timeout
            }
            else if (user.AntiSpam > now.AddMinutes(2))
            {
                user.AntiSpam = user.AntiSpam.AddMinutes(1); //<- Quicker Timeout
                shouldIssueWarning = true;
            }
            else
            {
                user.AntiSpam = user.AntiSpam.AddSeconds(10); //<- Add up message cost
            }

            // --- MESSAGE PROCESSING ---
            if (mayProcessMessage)
            {
                command = command.Trim();
                users.UpdateAntispam(user);
                OutboundMessage outboundMessage;

                //Gen Response
                switch (user.Activity)
                {
                    case ActivityContext.SETUP: outboundMessage = RespondSetup(user, command); break;
                    default: outboundMessage = RespondIdle(user, command); break;
                }

                //Should append warning?
                if (outboundMessage != null && shouldIssueWarning)
                {
                    outboundMessage.AddSticker("CAADAgADrQEAAnbw_gSHoLQMg52gHhYE"); //<- Nicht Witzig Sticker
                    outboundMessage.AddText(Texts.SPAM);
                }

                return outboundMessage;
            }

            return null;
        }
        private OutboundMessage RespondIdle(UserTable user, string command)
        {
            OutboundMessage msg = null;
            command = command.ToLower();
            string text;

            if (string.IsNullOrWhiteSpace(user.Class) || command == "/start")
            {
                msg = new OutboundMessage(user, Texts.START00);
                user.Activity = ActivityContext.SETUP;

                if (string.IsNullOrEmpty(user.Class))
                {
                    msg.AddText(Texts.START02);
                    msg.AddText(Texts.START03);
                    user.ActivityStep = 3;
                }
                else
                {
                    msg.AddText(Texts.Start01(user.Class));
                    msg.ReplyKeyboard = Keyboards.JA_NEIN;
                    user.ActivityStep = 1;
                }

                users.UpdateUser(user);
            }
            else
            {
                if (command.StartsWith("/vertretung"))
                {
                    string className = user.Class;
                    if (command != "/vertretung")
                    {
                        command = command.Remove(0, 11).Trim();
                        if (Regex.IsMatch(command, cfg.VplansClassRegex))
                        {
                            className = command;
                        }
                    }

                    msg = new OutboundMessage(user, vplaner.GetVPlan(className));
                    msg.InlineKeyboard = Keyboards.CallbackKlasse(className);
                }
                else if (command == "/hilfe")
                {
                    msg = new OutboundMessage(user, Texts.HILFE);
                }
                else if (command == "/erinnerung")
                {
                    user.Reminder = !user.Reminder;
                    text = user.Reminder ? "**EIN**" : "**AUS**";
                    msg = new OutboundMessage(user, Texts.ERINNERUNG + text);
                    users.UpdateUser(user);
                }
                else if (command == "/abbrechen")
                {
                    msg = new OutboundMessage(user, Texts.ABBRECHEN_NICHTS);
                }
                else if (command == "/zuruecksetzen")
                {
                    logger.Info($"Lösche {user.ID} auf Anfrage");
                    msg = new OutboundMessage(user, Texts.ZURÜCKSETZEN);
                    users.DeleteUser(user);
                }
                else if (command == "/info")
                {
                    msg = new OutboundMessage(user, Texts.INFO);

                    if (cfg.OperatorIdIsSet && user.ID == cfg.OperatorId)
                    {
                        msg.AddText($"User Count: {users.Count}");
                    }
                }
                else if (command.StartsWith("/klasse"))
                {
                    bool classValid = false;

                    if (command.Length > 9)
                    {
                        command = command.Remove(0, 7).Trim();
                        classValid = Regex.IsMatch(command, cfg.VplansClassRegex);
                    }

                    msg = new OutboundMessage(user.ID);
                    if (classValid)
                    {
                        string vplan = vplaner.GetVPlan(command);
                        if (user.Class != command)
                        {
                            user.Class = command;
                            users.UpdateUser(user);
                            vplan += string.Format(Texts.KLASSE_WECHSEL, command);
                        }

                        msg.AddText(vplan);
                        msg.InlineKeyboard = Keyboards.CallbackKlasse(command);
                    }
                    else
                    {
                        msg.AddText(Texts.KLASSE_FALSCH);
                    }
                }
            }

            return msg;
        }
        private OutboundMessage RespondSetup(UserTable user, string command)
        {
            OutboundMessage msg = null;

            if (command == "/abbrechen")
            {
                msg = new OutboundMessage(user, Texts.ABBRECHEN_OK);

                if (string.IsNullOrEmpty(user.Class))
                {
                    users.DeleteUser(user);
                }
                else
                {
                    user.Activity = ActivityContext.IDLE;
                    user.ActivityStep = 0;
                    users.UpdateUser(user);
                }
            }
            else
            {
                switch (user.ActivityStep)
                {
                    case 1: //<- Question if stored class is correct
                        command = command.ToLower();
                        if (command == "ja")
                        {
                            msg = new OutboundMessage(user, Texts.START11);
                            msg.AddText(Texts.START12);
                            msg.ReplyKeyboard = Keyboards.VERTRETUNG;
                            user.Activity = ActivityContext.IDLE;
                            user.ActivityStep = 0;
                        }
                        else
                        {
                            msg = new OutboundMessage(user, Texts.START08);
                            user.ActivityStep = 8;
                        }
                        break;

                    case 3: //Benutzernamenseingabe
                        user.ActivityStep = command == cfg.VplansUsername ? 5 : 4;
                        msg = new OutboundMessage(user, Texts.START04); //OR 05
                        break;

                    case 4: //Passworteingabe, aber mit falschem Benutzernamen
                        user.ActivityStep = 3;
                        msg = new OutboundMessage(user, Texts.START06);
                        msg.AddText(Texts.START03);
                        break;

                    case 5: //Passworteingabe
                        if (command == cfg.VplansPassword)
                        {
                            user.ActivityStep = 8;
                            msg = new OutboundMessage(user, Texts.START07);
                            msg.AddText(Texts.START08);
                        }
                        else
                        {
                            user.ActivityStep = 3;
                            msg = new OutboundMessage(user, Texts.START06);
                            msg.AddText(Texts.START03);
                        }
                        break;

                    case 8: //<- In welche Klasse gehst du?
                        command = command.ToLower();
                        if (Regex.IsMatch(command, cfg.VplansClassRegex))
                        {
                            msg = new OutboundMessage(user, Texts.START10);
                            msg.AddText(Texts.START12);
                            msg.ReplyKeyboard = Keyboards.VERTRETUNG;

                            user.Activity = ActivityContext.IDLE;
                            user.Class = command.ToLower();
                            user.ActivityStep = 0;
                        }
                        else
                        {
                            msg = new OutboundMessage(user, Texts.START09);
                            msg.AddText(Texts.START08);
                            user.ActivityStep = 8;
                        }
                        break;
                }

                users.UpdateUser(user);
            }

            return msg;
        }

        public void Shutdown() => this.bot.StopBot();
        ~Communicator() => Shutdown();
    }
}
