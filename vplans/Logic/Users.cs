﻿using Dapper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using vplans.Configuration;
using vplans.DB;

namespace vplans.Logic
{
    class Users
    {
        private readonly IDbConnection db;

        private static Users usersInstance;
        public static Users Instance { get => usersInstance ?? (usersInstance = new Users()); }

        public int Count { get { lock (db) { return db.QueryFirstOrDefault<int>(UserTable.COUNT); } } }

        private Users()
        {
            DBCore core = DBCore.Instance;
            db = core.db;

            if (core.importXml)
            {
                ReadXML();
            }
        }

        private void ReadXML()
        {
            string savePath = "./users.xml";
            if (File.Exists(savePath) && File.ReadAllText(savePath).Trim() != "")
            {
                XmlTextReader reader = new XmlTextReader(savePath);

                while (reader.Read())
                {
                    try
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name == "USER")
                            {
                                #region [USER]
                                long id = Convert.ToInt32(reader.GetAttribute("id"));
                                DateTime vplanLast = new DateTime();
                                DateTime lastText = new DateTime();

                                if (!bool.TryParse(reader.GetAttribute("oberstufe"), out bool oberstufe))
                                    oberstufe = false;

                                if (!bool.TryParse(reader.GetAttribute("erinnerung"), out bool erinnerung))
                                    erinnerung = true;

                                if (!DateTime.TryParse(reader.GetAttribute("vplanLast"), out vplanLast))
                                    vplanLast = DateTime.Now;

                                if (!DateTime.TryParse(reader.GetAttribute("lastSeen"), out lastText))
                                    lastText = DateTime.Now;

                                UserTable user = new UserTable
                                {
                                    ID = id,
                                    Class = reader.GetAttribute("jg"),
                                    LastSeen = lastText,
                                    Reminder = erinnerung
                                };

                                if (!Regex.IsMatch(user.Class, Config.GetInstance().VplansClassRegex))
                                {
                                    user.Class = "INVALID";
                                }

                                db.Execute(UserTable.INSERT, user);
                                #endregion
                            }
                        }
                    }
                    catch (Exception) { }
                }
                reader.Close();
            }
        }

        public UserTable GetUser(long userId)
        {
            lock (db)
            {
                UserTable user = db.QueryFirstOrDefault<UserTable>(UserTable.SELECT, new { ID = userId });

                if (user == null)
                {
                    user = new UserTable { ID = userId };
                    db.Execute(UserTable.INSERT, user);
                }

                return user;
            }
        }
        public UserTable[] GetObsoleteUsers()
        {
            DateTime minLastSeen = DateTime.Now.AddMonths(-16);
            lock (db)
            {
                return db.Query<UserTable>(UserTable.SELECT_OBSOLETE, new { MinLastSeen = minLastSeen.Ticks }).ToArray();
            }
        }
        public UserTable[] GetInvalidUsers()
        {
            lock (db)
            {
                return db.Query<UserTable>(UserTable.SELECT_INVALID).ToArray();
            }
        }


        public void UpdateUser(UserTable user)
        {
            lock (db)
            {
                db.Execute(UserTable.UPDATE, user);
            }
        }

        public void UpdateAntispam(UserTable user)
        {
            user.LastSeen = DateTime.Now;
            lock (db)
            {
                db.Execute(UserTable.UPDATE_ANTISPAM, user);
            }
        }
        public void DeleteUser(UserTable user)
        {
            lock (db)
            {
                db.Execute(UserTable.DELETE, user);
            }
        }

        public UserTable[] GetUsersToRemind(string[] classes)
        {
            StringBuilder statement = new StringBuilder("SELECT * FROM Users WHERE ReminderI = 1 AND (");
            bool firstCondition = true;
            foreach (string className in classes)
            {
                if (firstCondition)
                {
                    firstCondition = false;
                }
                else
                {
                    statement.Append(" OR ");
                }

                statement.Append($"Class LIKE '{className}%'");
            }
            statement.Append(");");

            lock (db)
            {
                return db.Query<UserTable>(statement.ToString()).ToArray();
            }
        }
    }
}
