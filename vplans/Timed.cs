﻿using System;

namespace vplans
{
    /// <summary>
    /// A wrapper class to add a local timestamp to any value
    /// </summary>
    /// <typeparam name="T">The object type to annotate with a timestamp</typeparam>
    public class Timed<T>
    {
        public T Value { get; private set; }

        public DateTime Timestamp { get; set; }

        public override string ToString() => $"Timed<{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz}>: {this.Value}";

        public static implicit operator Timed<T>(T value)
        {
            return new Timed<T>
            {
                Value = value,
                Timestamp = DateTime.Now
            };
        }

        public static explicit operator T(Timed<T> timed) => timed.Value;
    }
}
