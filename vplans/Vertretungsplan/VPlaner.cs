﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using vplans.Configuration;
using vplans.Logging;
using vplans.Logic;

namespace vplans.Vertretungsplan
{
    public class VPlaner
    {
        private List<VClass> vertretungen = new List<VClass>();
        private List<VClass> vertretungenTemp;
        private DateTime currentDate;
        private readonly Config cfg;

        private readonly string login;

        private readonly List<Timed<Exception>> toleratableExceptions = new List<Timed<Exception>>(10);
        private DateTime lastUpdateTime = DateTime.MinValue;

        public VPlaner()
        {
            cfg = Config.GetInstance();

            byte[] loginBytes = Encoding.UTF8.GetBytes(cfg.VplansUsername + ":" + cfg.VplansPassword);
            login = Convert.ToBase64String(loginBytes);
        }

        /// <summary>
        /// Updates the stored vPlans
        /// </summary>
        /// <param name="directoryPath">An optional directory path for tests</param>
        /// <returns>Returns the diff between two checks</returns>
        public string[] UpdatePlan(string directoryPath = null)
        {
            try
            {
                string[] diff = UpdatePlanInternal(directoryPath);
                toleratableExceptions.Clear();
                lastUpdateTime = DateTime.Now;
                return diff;
            }
            catch (Exception ex) when (ex is NullReferenceException || ex is WebException || ex is FormatException)
            {
                Logger.Instance.Warning($"Caught {ex?.GetType()} while updating VPLANS. No. {toleratableExceptions.Count} {ex}");

                lock (toleratableExceptions)
                {
                    toleratableExceptions.Add(ex);
                }

                if (toleratableExceptions.Count >= 10)
                {
                    Logger.Instance.Error("Too many tolerable exceptions");
                    foreach (Timed<Exception> pastEx in toleratableExceptions)
                    {
                        Logger.Instance.Error(pastEx);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Error(ex);
            }

            return new string[0];
        }

        /// <summary>
        /// Updates the stored vPlans
        /// </summary>
        /// <param name="directoryPath">An optional directory path for tests</param>
        /// <returns>Returns the diff between two checks</returns>
        /// <exception cref="WebException">When the plan could not be retrieved</exception>
        private string[] UpdatePlanInternal(string directoryPath = null)
        {
            int pageNumber = 1;
            vertretungenTemp = new List<VClass>();

            while (true)
            {
                HtmlDocument html;

                if (string.IsNullOrEmpty(directoryPath))
                {
                    html = GetWebPlan(pageNumber);
                }
                else
                {
                    html = new HtmlDocument();
                    html.Load(Path.Combine(directoryPath, $"subst_{pageNumber:D3}.htm"), Encoding.GetEncoding("ISO-8859-1"));
                }

                string nextPageString = html.DocumentNode.SelectSingleNode("//meta[@http-equiv='refresh']").Attributes[1].Value;
                nextPageString = Regex.Match(nextPageString, "(?<=subst_).+?(?=\\.htm)").Value;
                int nextPage = int.Parse(nextPageString);

                string currentDateString = html.DocumentNode.SelectSingleNode("//div[@class='mon_title']").InnerText;
                currentDate = DateTime.Parse(Regex.Match(currentDateString, ".+?(?=\\s)").Value);

                if (!html.DocumentNode.InnerText.Contains("Keine Vertretungen"))
                {
                    bool readingHeaders = true;
                    string[] headers = null;
                    string[] classSeperators = new string[] { ", " };

                    foreach (HtmlNode hEntry in html.DocumentNode.SelectNodes("//center[1]//table[@class=\"mon_list\"]//tr"))
                    {
                        #region [GET HEADERS]
                        if (readingHeaders)
                        {
                            List<string> headerList = new List<string>(9);
                            foreach (HtmlNode htmlNode in hEntry.ChildNodes)
                            {
                                headerList.Add(htmlNode.InnerText.Replace("(Klasse(n))", "Klasse"));
                            }
                            headers = headerList.ToArray();
                            readingHeaders = false;
                            continue;
                        }
                        #endregion

                        #region [GET ROW]
                        string[] vEntryInfosInitial = new string[headers.Length];
                        int cellIndex = 0;
                        foreach (HtmlNode childNode in hEntry.ChildNodes)
                        {
                            vEntryInfosInitial[cellIndex] = childNode.InnerText;
                            cellIndex++;
                        }

                        vEntryInfosInitial[0] = Regex.Replace(vEntryInfosInitial[0], @"(Jg )|(\()|(\))|(0(?=\d))", "");
                        #endregion

                        #region [ADD ROW]
                        if (vEntryInfosInitial[0].Contains(", "))
                        {
                            #region <Explanation>
                            /*                 -- Before: --
                                +============+============+=================+
                                |   Klasse   | Vertretung | Was weiß ich... |
                                +============+============+=================+       >---------,
                                | 06, 07, 08 | TERF       | Aufg. Dyll      |                  |
                                +------------+------------+-----------------+                   |
                                                                                                |
                                             -- After: --                                       |
                                +========+============+=================+                       |
                                | Klasse | Vertretung | Was weiß ich... |                       |
                                +========+============+=================+                       |
                                |     06 | TERF       | Aufg. Dyll      |                      |
                                +--------+------------+-----------------+ <-------------------'
                                |     07 | TERF       | Aufg. Dyll      |
                                +--------+------------+-----------------+
                                |     08 | TERF       | Aufg. Dyll      |
                                +--------+------------+-----------------+
                             */
                            #endregion
                            foreach (string className in vEntryInfosInitial[0].Split(classSeperators, StringSplitOptions.RemoveEmptyEntries))
                            {
                                AppendEntry(headers, vEntryInfosInitial, className);
                            }
                        }
                        else
                        {
                            AppendEntry(headers, vEntryInfosInitial, vEntryInfosInitial[0]);
                        }
                        #endregion
                    }
                }

                if (nextPage <= pageNumber)
                {
                    break;
                }
                pageNumber = nextPage;
            }

            List<string> diff = new List<string>();
            lock (vertretungen)
            {
                foreach (VClass vc in vertretungen)
                {
                    if (!vertretungenTemp.Contains(vc))
                    {
                        diff.Add(vc.ClassName);
                    }
                }

                foreach (VClass vc in vertretungenTemp)
                {
                    if (!vertretungen.Contains(vc) && !diff.Contains(vc.ClassName))
                    {
                        diff.Add(vc.ClassName);
                    }
                }

                vertretungen = vertretungenTemp;
            }

            return diff.ToArray();
        }

        private void AppendEntry(string[] headers, string[] vEntryInfosInitial, string className)
        {
            string trimmedClassName = className.TrimStart('0').ToLower();
            VClass vClass = (from lqi in vertretungenTemp where lqi.Date == currentDate && lqi.ClassName == trimmedClassName select lqi).FirstOrDefault();
            if (vClass == null)
            {
                vertretungenTemp.Add(new VClass(trimmedClassName, currentDate, headers, vEntryInfosInitial));
            }
            else
            {
                vClass.Add(vEntryInfosInitial);
            }
        }

        /// <summary>
        /// Called from the Communicator, to get the freshest vPlan!
        /// </summary>
        /// <param name="classId">The class to get the vPlan for</param>
        /// <returns>A formatted vPlan</returns>
        public string GetVPlan(string classId)
        {
            if (toleratableExceptions.Count > 1 || DateTime.Now.Subtract(new TimeSpan(2, 0, 0)) > lastUpdateTime)
            {
                return Texts.WEBPLAN_ERROR;
            }

            List<VClass> vEntries = new List<VClass>();
            lock (vertretungen)
            {

                //Try find exact class e.g. (7b, 10c, EF, Q2)
                ConcatClasses(classId, vEntries);

                //Try find generic class e.g. (7, 10)
                Match match = Regex.Match(classId, cfg.VplansSpecificClassRegex);
                if (match.Success)
                {
                    ConcatClasses(match.Value, vEntries);
                }
            }

            if (vEntries.Count > 0)
            {
                IEnumerable<VClass> vEntriesOrdered = from lqi in vEntries orderby lqi.Date ascending select lqi;
                return string.Concat(vEntriesOrdered);
            }
            else
            {
                return "`Keine Vertretung für demnächst gefunden`";
            }
        }

        private void ConcatClasses(string classId, List<VClass> vEntries)
        {
            IEnumerable<VClass> vClasses = from vClass in vertretungen where vClass.ClassName == classId select vClass;
            foreach (VClass vClass in vClasses)
            {
                VClass vClassDate = (from lqi in vEntries where lqi.Date == vClass.Date select lqi).FirstOrDefault();
                if (vClassDate == null)
                {
                    VClass vEntry = new VClass();
                    vEntry.Add(vClass);
                    vEntries.Add(vEntry);
                }
                else
                {
                    vClassDate.Add(vClass);
                }
            }
        }

        private HtmlDocument GetWebPlan(int pageNumber)
        {
            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, string.Format(cfg.VplansUrl, pageNumber));

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent", "vplans_bot");
            client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*; q=0.8");
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + login);

            HtmlDocument html = new HtmlDocument();

            HttpResponseMessage res = client.Send(msg);
            using (Stream stream = res.Content.ReadAsStream())
            {
                html.Load(stream, Encoding.GetEncoding("ISO-8859-1"));
            }

            return html;
        }
    }
}
