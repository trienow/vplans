﻿using System;

namespace vplans.Configuration
{
    class ConfigurationException : Exception
    {
        public ConfigurationException(string message) : base(message) { }
    }
}
