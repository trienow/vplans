﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using vplans.Logging;
using vplans.Logic;
using vplans.Vertretungsplan;

namespace vplans
{
    class Program
    {
        private const int CHECK_MINUTES = 10;

        #region [SHOW/HIDE CONSOLE]
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

#pragma warning disable IDE0051 // Nicht verwendete private Member entfernen
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;
#pragma warning restore IDE0051 // Nicht verwendete private Member entfernen
        #endregion

        static void Main()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13;

            Exception configException = null;
            Configuration.Config cfg = null;
            try
            {
                cfg = Configuration.Config.GetInstance();
            }
            catch(Exception ex)
            {
                configException = ex;
            }

            Logger.SetupLogger(cfg);
            ILogger log = Logger.Instance;

            try
            {
                //If loading the config failed, let's now log it to the logger
                if (configException != null)
                {
                    throw configException;
                }

                VPlaner planer = new VPlaner();
                Communicator com = new Communicator(planer);

                planer.UpdatePlan();

                DateTime nextCleanup = DateTime.MinValue;
                DateTime lastCleanupSuccess = DateTime.MinValue;
                //Write down which logs we transfered to not transfer them again.
                HashSet<string> successfulErrorTransfers = new HashSet<string>();

                if (!cfg.ShowConsole)
                {
                    ShowWindow(GetConsoleWindow(), SW_HIDE);
                }

                int sleepMs;
                while (!com.ShutdownRequested)
                {
                    log.Info("Updating...");
                    string[] needUpdates = planer.UpdatePlan();

                    log.Info("Notifying...");
                    com.NotifyClasses(needUpdates);

                    DateTime now = DateTime.Now;
                    try
                    {
                        #region [CLEANUP]
                        if (now > nextCleanup)
                        {
                            if (cfg.OperatorIdIsSet)
                            {
                                log.Info("Error transfer time...");
                                try
                                {
                                    //See if Serilog deleted any logs 
                                    successfulErrorTransfers.RemoveWhere((f) => !File.Exists(f));

                                    foreach (string errFile in Logger.GetErrorFiles())
                                    {
                                        if (!successfulErrorTransfers.Contains(errFile))
                                        {
                                            com.SendDebugFile(errFile);
                                            successfulErrorTransfers.Add(errFile);
                                        }
                                    }

                                    lastCleanupSuccess = now;
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex);
                                }

                                if (lastCleanupSuccess != now)
                                {
                                    //In case the files could not be sent (for example)
                                    com.SendDebugText("WARNING! ERROR TRANSFER ATTEMPTS FAILED");
                                }
                            }

                            log.Info("Purging old users...");
                            com.PurgeObsoleteUsers();

                            nextCleanup = now.Date.AddDays(1);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }

                    #region [SLEEP]
                    //INFO: When changing the sleep time, adjust the timeout in VPlaner.GetVplan
                    int h = now.Hour;
                    if (h >= 5 && h < 19)
                    {
                        sleepMs = (CHECK_MINUTES - (now.Minute % CHECK_MINUTES)) * 60000;
                        sleepMs -= now.Second * 1000 + now.Millisecond;
                    }
                    else
                    {
                        sleepMs = 6 * CHECK_MINUTES * 60000;
                    }
                    log.Info($"Sleeping until {DateTime.Now.AddMilliseconds(sleepMs)}...");
                    Thread.Sleep(sleepMs);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
