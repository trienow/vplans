﻿using Dapper;
using System;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace vplans.DB
{
    public class DBCore : IDisposable
    {
        public const string DB_FILE = "./users.db";
        private const string XML_FILE = "./users.xml";

        public readonly IDbConnection db;
        public readonly bool importXml = false;

        private static DBCore coreInstance = null;
        public static DBCore Instance { get => coreInstance ?? (coreInstance = new DBCore()); }

        private DBCore()
        {
            importXml = !File.Exists(DB_FILE) && File.Exists(XML_FILE);

            if (File.Exists(DB_FILE + "-journal"))
            {
                File.Delete(DB_FILE + "-journal");
            }

            db = new SQLiteConnection("DataSource=" + DB_FILE);
            db.Open();
            InitTables();
        }

        private void InitTables()
        {
            db.Execute(MetaTable.CREATE);
            db.Execute(UserTable.CREATE);

            MetaTable meta = db.QueryFirstOrDefault<MetaTable>("SELECT * FROM Meta");
            if (meta == null)
            {
                meta = new MetaTable();
                db.Query(MetaTable.INSERT, meta);
            }

            //VERSION UPGRADE HERE
        }

        public void Dispose()
        {
            try
            {
                lock (this)
                {
                    if (db.State == ConnectionState.Open)
                    {
                        db.Close();
                    }

                    db.Dispose();
                }
            }
            catch
            {
            }
        }

        public IDbTransaction StartTransaction() => db.BeginTransaction(IsolationLevel.Serializable);

        ~DBCore() => Dispose();
    }
}
