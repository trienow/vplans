﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using vplans.Configuration;
using vplans.Logging;

namespace vplans.Telegrammer
{
    internal class VBot : IUpdateHandler
    {
        private readonly TelegramBotClient bot;
        private readonly CancellationTokenSource cts = new CancellationTokenSource();
        private bool isReceiving = false;
        private DateTime ignoreMessagesBeforeUTC = DateTime.MaxValue;

        private readonly ILogger logger = Logger.Instance;
        private readonly Config cfg = Config.GetInstance();

        public event EventHandler<InboundEventArgs> InboundEvent;
        public readonly LinkedList<Timed<string>> toleratableExceptions = new LinkedList<Timed<string>>();

        #region [INIT]
        public VBot()
        {
            this.bot = new TelegramBotClient(cfg.BotToken);
        }

        public Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type == UpdateType.Message)
            {
                if (update.Message.Date > ignoreMessagesBeforeUTC)
                {
                    if (update.Message.Type == MessageType.Text)
                    {
                        logger.Info($"[{update.Message.Chat.Id}] -M> {update.Message.Text}");
                        InboundEvent?.Invoke(this, new InboundEventArgs(update.Message.Chat.Id, update.Message.Text));
                    }
                }
            }
            else if (update.Type == UpdateType.CallbackQuery)
            {
                if (DateTime.UtcNow > ignoreMessagesBeforeUTC)
                {
                    CallbackQuery cq = update.CallbackQuery;
                    string command = cq?.Data;
                    if (!string.IsNullOrWhiteSpace(command) && command[0] == '/')
                    {
                        logger.Info($"[{cq.Message.Chat.Id}] -C> {command}");
                        InboundEvent?.Invoke(this, new InboundEventArgs(cq.Message.Chat.Id, command) { SendingQuery = cq });
                    }
                }
            }

            return Task.CompletedTask;
        }

        public Task HandleErrorAsync(ITelegramBotClient botClient, Exception ex, CancellationToken cancellationToken)
        {
            if (ex is ApiRequestException arex)
            {
                if (arex.ErrorCode != 408)
                {
                    logger.Error($"Bot_OnReceiveError {arex}");
                }
            }
            else if (ex is RequestException rex)
            {
                Exception rexInner = rex.InnerException;
                bool handled = false;

                if (rexInner is WebException wex && wex.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    logger.Warning("Bot_OnReceiveGeneralError: NameResolutionFailure");
                }
                else if (rexInner is System.Net.Http.HttpRequestException hrex)
                {
                    string exMessage = hrex.Message;
                    if (exMessage.Contains("502") || exMessage.Contains("500"))
                    {
                        logger.Warning("Bot_OnReceiveGeneralError: 500|502");
                        handled = true;
                    }
                }

                if (!handled)
                {
                    //Clean out old exceptions
                    DateTime now1h = DateTime.Now.AddHours(-2);
                    LinkedListNode<Timed<string>> node = toleratableExceptions.First;
                    while (node != null && node.Value.Timestamp < now1h)
                    {
                        toleratableExceptions.RemoveFirst();
                        node = toleratableExceptions.First;
                    }

                    //Check and see if a socket exception can be found...
                    Exception innerException = rexInner;
                    while (innerException != null && !(innerException is System.Net.Sockets.SocketException))
                    {
                        innerException = innerException.InnerException;
                    }

                    //Add current exception to the list
                    string text;
                    if (innerException is System.Net.Sockets.SocketException sox)
                    {
                        text = $"Bot_OnReceiveGeneralError SocketException {sox.SocketErrorCode}: {ex}";
                    }
                    else
                    {
                        text = $"Bot_OnReceiveGeneralError {ex}";
                    }

                    logger.Warning($"Suppressing exception #{toleratableExceptions.Count}: {text}");
                    toleratableExceptions.AddLast(text);

                    //Only dump them into the log, if we accumulated more then 9 exceptions in the last 2h
                    if (toleratableExceptions.Count > 9)
                    {
                        foreach (Timed<string> exception in toleratableExceptions)
                        {
                            logger.Error(exception);
                        }
                        toleratableExceptions.Clear();
                    }
                }
            }

            Thread.Sleep(60_000);
            ignoreMessagesBeforeUTC = DateTime.UtcNow.AddSeconds(10);
            return Task.CompletedTask;
        }

        public bool StartBot()
        {
            bool result = true;

            if (!isReceiving)
            {
                isReceiving = true;
                ReceiverOptions rxOpt = new ReceiverOptions
                {
                    AllowedUpdates = new UpdateType[] { UpdateType.Message, UpdateType.CallbackQuery },
                    ThrowPendingUpdates = false
                };
                this.bot.StartReceiving(this, rxOpt, cts.Token);
                ignoreMessagesBeforeUTC = DateTime.UtcNow.AddSeconds(10);

                if (cfg.OperatorIdIsSet)
                {
                    SendMessage(new OutboundMessage(cfg.OperatorId, "Bot online!"));
                }
            }

            return result;
        }
        #endregion

        #region [ON MESSAGE]
        /// <summary>
        /// Sends a message to a specific user
        /// </summary>
        /// <param name="userId">The user ID of the user to send a message to</param>
        /// <param name="message">The message to send to the user</param>
        public void SendMessage(OutboundMessage m)
        {
            if (m != null)
            {
                int lastMessage = m.Texts.Count - 1;
                for (int i = 0; i < m.Texts.Count; i++)
                {
                    MessageAnimation ma = m.Texts[i];
                    if (i > 0)
                    {
                        bot.SendChatActionAsync(new ChatId(m.UserId), ma.DelayAction);
                    }

                    Thread.Sleep(ma.Delay);
                    logger.Info($"[{m.UserId}] <S- {ma}");

                    IReplyMarkup replyMarkup = i == lastMessage ? m.ReplyKeyboard : null;

                    switch (ma.MessageType)
                    {
                        case MessageType.Text:
                            bot.SendTextMessageAsync(new ChatId(m.UserId), $"{ma}", ma.ParseMode, replyMarkup: replyMarkup);
                            break;

                        case MessageType.Sticker:
                            bot.SendStickerAsync(new ChatId(m.UserId), new InputOnlineFile(ma), replyMarkup: replyMarkup);
                            break;

                        case MessageType.Document:
                            FileStream fs = System.IO.File.Open(ma, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            InputOnlineFile iof = new InputOnlineFile(fs, Path.GetFileName(ma));
                            Task<Message> msg = bot.SendDocumentAsync(new ChatId(m.UserId), iof, replyMarkup: replyMarkup);
                            msg.Wait();
                            break;
                    }
                }
            }
        }

        public void AnswerCallback(OutboundMessage m)
        {
            if (m != null && m.Texts.Count > 0)
            {
                CallbackQuery q = m.RespondingToQuery;
                bot.AnswerCallbackQueryAsync(q.Id);
                MessageAnimation first = m.Texts[0];

                logger.Info($"[{q.Message.Chat.Id}] <E- {first}");
                bot.EditMessageTextAsync(q.Message.Chat, q.Message.MessageId, first, first.ParseMode, replyMarkup: m.InlineKeyboard);

                //If we have more messages, send them individually
                if (m.Texts.Count > 1)
                {
                    m.Texts.RemoveAt(0);
                    SendMessage(m);
                }
            }
        }
        #endregion

        #region [STOPPING]
        ~VBot() => this.StopBot();

        public void StopBot()
        {
            cts.Cancel();
        }
        #endregion
    }
}
