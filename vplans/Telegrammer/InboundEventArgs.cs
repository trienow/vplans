﻿using System;
using Telegram.Bot.Types;

namespace vplans.Telegrammer
{
    class InboundEventArgs : EventArgs
    {
        public long ChatId { get; set; }
        public string Message { get; set; }
        public CallbackQuery SendingQuery { get; set; } = null;

        public InboundEventArgs(long chatId, string message)
        {
            this.ChatId = chatId;
            this.Message = message;
        }
    }
}
