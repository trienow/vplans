﻿using Telegram.Bot.Types.ReplyMarkups;

namespace vplans.Telegrammer
{
    public static class Keyboards
    {
        public static InlineKeyboardMarkup CallbackKlasse(string klasse) => new InlineKeyboardMarkup(InlineKeyboardButton.WithCallbackData(klasse + " aktualisieren", "/vertretung " + klasse));
        public static IReplyMarkup JA_NEIN
        {
            get =>
                new ReplyKeyboardMarkup(new KeyboardButton[][]
                {
                    new KeyboardButton[] { new KeyboardButton("Ja") },
                    new KeyboardButton[] { new KeyboardButton("Nein") }
                })
                {
                    OneTimeKeyboard = true,
                    ResizeKeyboard = true,
                };
        }

        public static IReplyMarkup CLEAR { get => new ReplyKeyboardRemove(); }
        public static IReplyMarkup VERTRETUNG
        {
            get => new ReplyKeyboardMarkup(new KeyboardButton[] { new KeyboardButton("/vertretung") })
            {
                OneTimeKeyboard = true,
                ResizeKeyboard = true,
            };
        }
        public static IReplyMarkup START
        {
            get => new ReplyKeyboardMarkup(new KeyboardButton[] { new KeyboardButton("/start") })
            {
                OneTimeKeyboard = true,
                ResizeKeyboard = true,
            };
        }
    }
}
