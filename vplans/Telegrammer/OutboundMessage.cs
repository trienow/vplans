﻿using System.Collections.Generic;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using vplans.DB;

namespace vplans.Telegrammer
{
    public class OutboundMessage
    {
        #region [GENERIC]
        public long UserId { get; set; }
        public CallbackQuery RespondingToQuery { get; set; }
        public List<MessageAnimation> Texts { get; set; } = new List<MessageAnimation>();
        #endregion

        #region [KEYBOARDS]
        public InlineKeyboardMarkup InlineKeyboard { get; set; } = null;
        public IReplyMarkup ReplyKeyboard { get => this.InlineKeyboard != null ? InlineKeyboard : replyMarkup; set => replyMarkup = value; }
        private IReplyMarkup replyMarkup = Keyboards.CLEAR;
        #endregion

        #region [FUNCTIONS]
        public OutboundMessage(long userId)
        {
            UserId = userId;
        }
        public OutboundMessage(long userId, string text) : this(userId)
        {
            Texts.Add(new MessageAnimation(text));
        }
        public OutboundMessage(UserTable user, string text) : this(user.ID, text) { } 

        public void AddText(string text, ParseMode? parseMode = ParseMode.Markdown)
        {
            MessageAnimation mAni = new MessageAnimation(text) { ParseMode = parseMode };

            if (Texts.Count > 0)
            {
                mAni.Delay = 1000;
            }

            Texts.Add(mAni);
        }

        public void AddSticker(string content) => this.Texts.Add(new MessageAnimation(content) { MessageType = MessageType.Sticker });

        public void AddDocument(string document) => this.Texts.Add(new MessageAnimation(document) { MessageType = MessageType.Document });
        #endregion
    }
}
