﻿using System.Collections.Generic;

namespace vplans
{
    class ArrayEqualityComparer<T> : IEqualityComparer<T[]>
    {
        public bool Equals(T[] x, T[] y)
        {
            bool equals = ReferenceEquals(x, y) || (x is T[] && y is T[] && x.Length == y.Length);

            if (equals)
            {
                for (int i = 0; i < y.Length; i++)
                {
                    T xi = x[i];
                    T yi = y[i];

                    if (!(ReferenceEquals(xi, yi) || (xi is T && yi is T && xi.Equals(yi))))
                    {
                        equals = false;
                        break;
                    }
                }
            }

            return equals;
        }

        public int GetHashCode(T[] arr)
        {
            int h = 48358732;

            foreach (T item in arr)
            {
                h *= arr.GetHashCode();
            }

            return h;
        }
    }
}
